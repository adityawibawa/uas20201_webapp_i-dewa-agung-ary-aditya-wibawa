from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Toko(models.Model):
    
    NamaToko = models.CharField(max_length=50)
    Alamat = models.CharField(max_length=50)
    Pemilik = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.NamaToko)


class Produk(models.Model):
    
    NamaProduk = models.CharField(max_length=50)
    Harga = models.IntegerField()
    ImageProduk = models.FileField()
    Keterangan = models.CharField(max_length=50)
    NamaToko = models.ForeignKey(Toko, on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.NamaProduk)  