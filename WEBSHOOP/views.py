from django.shortcuts import render, redirect
from WEBSHOOP import models, forms
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# Create your views here.

def HomeViews(request):
    return render(request, 'Home.html')

def TokoViews(request):
    toko = models.Toko.objects.all()
    
    context = {
        'toko': toko,
    }

    return render(request, 'Toko.html', context)

def ProdukViews(request):
    produk = models.Produk.objects.all()

    context = {
        'produk': produk,

    }
    return render(request, 'Produk.html', context)

class TokoCreate(CreateView):
    template_name = "TokoForm.html"
    form_class = forms.Toko

    def get_success_url(self):
        return reverse_lazy('toko')

class TokoUpdate(UpdateView):
    template_name = "TokoForm.html"
    form_class = forms.Toko
    model = models.Toko

    def get_success_url(self):
        return reverse_lazy('toko')

class TokoDelete(DeleteView):
    template_name = "DeleteConfirmation.html"
    form_class = forms.Toko
    model = models.Toko

    def get_success_url(self):
        return reverse_lazy('toko')


class ProdukCreate(CreateView):
    template_name = "ProdukForm.html"
    form_class = forms.Produk

    def get_success_url(self):
        return reverse_lazy('produk')

class ProdukUpdate(UpdateView):
    template_name = "ProdukForm.html"
    form_class = forms.Produk
    model = models.Produk

    def get_success_url(self):
        return reverse_lazy('produk')

class ProdukDelete(DeleteView):
    template_name = "DeleteConfirmation.html"
    form_class = forms.Produk
    model = models.Produk

    def get_success_url(self):
        return reverse_lazy('produk')