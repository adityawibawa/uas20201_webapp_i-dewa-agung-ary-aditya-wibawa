from django.forms import ModelForm
from django import forms
from .models import *

class Toko(ModelForm):
    class Meta:
        model = Toko
        fields = '__all__'

class Produk(ModelForm):
    class Meta:
        model = Produk
        fields = '__all__'