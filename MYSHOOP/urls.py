"""MYSHOOP URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from WEBSHOOP import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.HomeViews, name='home'),
    path('Toko', views.TokoViews ,name='toko'),
    path('Produk', views.ProdukViews ,name='produk'),
    path('Toko/create', views.TokoCreate.as_view() ,name='tokocreate'),
    path('Toko/update/<int:pk>', views.TokoUpdate.as_view() ,name='tokoupdate'),
    path('Toko/delete/<int:pk>', views.TokoDelete.as_view() ,name='tokodelete'),
    path('Produk/create', views.ProdukCreate.as_view() ,name='produkcreate'),
    path('Produk/update/<int:pk>', views.ProdukUpdate.as_view() ,name='produkupdate'),
    path('Produk/delete/<int:pk>', views.ProdukDelete.as_view() ,name='produkdelete'),
]
